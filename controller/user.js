var express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
require("../model/user.model");
const UserModel = mongoose.model("Users");
const UserRoleModel = mongoose.model("UserRole");

const configuration = require("../server/configuration");
const referenceService = require("../services/ReferenceServices");
let scripts = referenceService.LoadScript("user");
let styles = referenceService.LoadStyle("user");

router
  .get("/", function(req, res) {
    UserModel.find((err, data) => {
      if (err) return err;
      console.log(data);
      res.render("user", { users: data, styles: styles, scripts: scripts });
    });
  })
  .get("/getAllUsers", function(req, res) {
    UserModel.find(
      {},
      { userName: 1, mobile: 1, createdDate: 1, _id: 0, role: 1 },
      (err, data) => {
        if (err) return err;
        res.status(200).json(data);
      }
    );
  })
  .get("/deleteAllUsers", function(req, res) {
    UserModel.deleteMany((err, data) => {
      if (err) return err;
      res.status(200).json(data);
    });
  })
  .get("/dump", async (req, res) => {
    try {
      var data = await UserModel.find().exec();
      res.status(200).json(data);
    } catch (error) {
      res.status(500).json(error);
    }
  })
  .get("/getRoles", function(req, res) {
    UserRoleModel.find({}, { role: 1, _id: 0 }, (err, data) => {
      if (err) return err;
      res.status(200).json(data);
    });
  });

module.exports = router;
