var express = require("express");
const mongoose = require("mongoose");
const Bcrypt = require("bcryptjs");
let salt = Bcrypt.genSaltSync(10);
const router = express.Router();
require("../model/user.model");
const UserModel = mongoose.model("Users");
const UserRoleModel = mongoose.model("UserRole");

const referenceService = require("../services/ReferenceServices");
const configuration = require("../server/configuration");
let scripts = referenceService.LoadScript("login");
let styles = referenceService.LoadStyle("login");

router
  .get("/", function(req, res) {
    res.render("Login", { title: "Login", styles: styles, scripts: scripts });
  })
  .post("/newUser", async function(req, res) {
    var tabParams = req.body;
    var user = new UserModel();
    var userRole = new UserRoleModel();
    await UserModel.countDocuments((err, data) => {
      if (err) return err;
      if (data === 0) {
        user.role = "Admin";
        userRole.role = "Admin";
        userRole.save();
      }
    });
    let hash = Bcrypt.hashSync(tabParams.password, salt);
    user.userName = tabParams.username;
    user.hash = hash;
    user.mobile = tabParams.mobile;
    await user.save((err, data) => {
      if (err) {
        console.log(err);
        return res.status(500).json(err);
      }
      res.status(200).json(data);
    });
  })
  .post("/SignIn", async function(req, res) {
    var tabParams = req.body;
    var userSession = req.session;
    try {
      var user = await UserModel.findOne({
        userName: tabParams.username
      }).exec();
      if (!user) {
        return res.status(400).json({ message: "The username does not exist" });
      }
      if (!Bcrypt.compareSync(tabParams.password, user.hash)) {
        return res.status(400).json({ message: "The password is invalid" });
      }
      req.session[configuration.sessionKeys.userName] = user.userName;
      return res.status(200).json({
        message: "The username and password combination is correct!"
      });
      // return res.redirect("/Dashboard");
    } catch (error) {
      res.status(500).json(error);
    }
  });

module.exports = router;
