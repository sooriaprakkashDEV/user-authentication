(function($) {
  "use strict";
  $("#btnSignUpSubmit").on("click", function(ele) {
    var check = true;
    var input = $(".validate-input .inputSignUp");
    for (var i = 0; i < input.length; i++) {
      if (validate(input[i]) == false) {
        showValidate(input[i]);
        check = false;
      }
    }
    if (check) Signup(ele);
    return check;
  });
  $(".validate-form").on("submit", function(ele) {
    ele.preventDefault(); // or return false, your choice
    var check = true;
    var input = $(".validate-input .inputSignIn");
    for (var i = 0; i < input.length; i++) {
      if (validate(input[i]) == false) {
        showValidate(input[i]);
        check = false;
      }
    }
    if (check) {
      $.post(ele.currentTarget.action, $(this).serialize())
        .done(function(data) {
          NotifyMessage(data.message, "success");
          showLogin();
          document.location.href = "/Dashboard";
          ResetSignUp();
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          NotifyMessage(jqXHR.responseJSON.message || errorThrown, "error");
        });
    }
    return check;
  });
  $("#toSignup").on("click", function() {
    showSignUp();
  });
  $("#toLogin").on("click", function() {
    showLogin();
  });
  $(".validate-form .input100").each(function() {
    $(this).focus(function() {
      hideValidate(this);
    });
  });
  function validate(input) {
    if ($(input).attr("type") == "email" || $(input).attr("name") == "email") {
      if (
        $(input)
          .val()
          .trim()
          .match(
            /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/
          ) == null
      ) {
        return false;
      }
    }
    if ($(input).attr("type") == "tel" || $(input).attr("name") == "mobile") {
      if (
        !$(input)
          .val()
          .match("[0-9]{10}")
      ) {
        return false;
      }
    } else {
      if (
        $(input)
          .val()
          .trim() == ""
      ) {
        return false;
      }
    }
  }
  function showValidate(input) {
    var thisAlert = $(input).parent();
    $(thisAlert).addClass("alert-validate");
  }
  function hideValidate(input) {
    var thisAlert = $(input).parent();
    $(thisAlert).removeClass("alert-validate");
  }
  function showSignUp() {
    $("#frmLogin").hide();
    $("#frmSignup").show();
  }
  function showLogin() {
    $("#frmSignup").hide();
    $("#frmLogin").show();
  }
  showLogin();
  function Signup(ele) {
    $.post("/Login/newUser", $("#frmSignup").serialize())
      .done(function(data) {
        NotifyMessage("Successfully Registered!", "success");
        showLogin();
        ResetSignUp();
      })
      .fail(function(error) {
        NotifyMessage("Registeration Failed!", "error");
      });
  }
  function ResetSignUp() {
    $(".validate-input .inputSignUp").val("");
  }
  function ResetSignIn() {
    $(".validate-input .inputSignIn").val("");
  }
})(jQuery);
