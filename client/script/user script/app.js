const NotifyMessage = function(message, type) {
  swal({
    text: message,
    type: type,
    buttons: false,
    timer: 3000
  });
};
