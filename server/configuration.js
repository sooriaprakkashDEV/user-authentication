const port = process.env.PORT || 3001;
const environment = "DEV"; //DEV || QAT || UAT || PROD
const authorize = function(req, res, next) {
  var userSession = req.session;
  console.log(req.session);
  if (userSession[sessionKeys.userName]) return next();
  else return res.status(401).json({ message: "Unauthorized access" });
};

//Session Keys
var sessionKeys = {
  userName: "userName"
};

exports.port = port;
exports.environment = environment;
exports.authorize = authorize;
exports.sessionKeys = sessionKeys;
