# Appiness
Task 1
Making the first user who registers, as Admin.

### Tech

It uses a number of open source projects to work properly:

* [VS Code] - awesome web-based text editor
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js web app framework
* [jQuery] - The Write Less, Do More, JavaScript Library.


### Installation

Install the dependencies and start the server.

```sh
$ cd appiness
$ npm install
$ node index
```

Verify the development by navigating to your server address in your preferred browser.

```sh
127.0.0.1:3001
```

Some Routes are
```sh
http://localhost:3001/Dashboard
http://localhost:3001/Login
http://localhost:3001/Dashboard/deleteAllUsers
http://localhost:3001/Dashboard/getRoles
```
