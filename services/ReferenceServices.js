const config = require("../server/configuration");

const LoadScript = pageName => {
  let scripts = [];
  if (config.environment === "DEV") {
    scripts.push("script/plugins/jquery-3.4.1.min.js");
    scripts.push("script/plugins/animsition.min.js");
    scripts.push("script/plugins/popper.min.js");
    scripts.push("script/plugins/bootstrap.min.js");
    scripts.push("script/plugins/sweetalert.min.js");
    scripts.push("script/user script/app.js");

    switch (pageName) {
      case "user":
        scripts.push("script/user script/user.js");
        break;
      case "login":
        scripts.push("script/user script/login.js");
        break;
    }
  }
  return scripts;
};

const LoadStyle = pageName => {
  let styles = [];
  if (config.environment === "DEV") {
    styles.push("style/plugins/animsition.min.css");
    styles.push("fonts/font-awesome-4.7.0/css/font-awesome.min.css");
    styles.push("style/plugins/bootstrap.min.css");
    styles.push("style/user style/custom.css");

    // switch (pageName) {
    //   case "user":
    //     styles.push("style/user style/user");
    //     break;
    // }
  }
  return styles;
};

exports.LoadScript = LoadScript;
exports.LoadStyle = LoadStyle;
