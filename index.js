const connection = require("./model");
require("./model/user.model");
const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const cons = require("consolidate");
const path = require("path");

const users = require("./controller/user");
const login = require("./controller/login");

const app = express();
app.set("trust proxy", 1); // trust first proxy
app.use(
  session({
    secret: "appiness",
    name: "appiness",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: true }
  })
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname + "/client")));

app.engine("html", cons.swig);
app.set("view engine", "html");
app.set("views", path.join(__dirname + "/views"));

app.use("/Dashboard", users);
app.use("/Login", login);

app.listen("3001", () => {
  console.log("Server on 3001");
});
