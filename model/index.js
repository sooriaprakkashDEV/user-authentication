const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost:27017/appiness", {
    useUnifiedTopology: true,
    useCreateIndex: true,
    useNewUrlParser: true
  })
  .then(() => console.log("Mongo DB Connected"))
  .catch(err => {
    console.log(`DB Connection Error: ${err.message}`);
  });
