const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Userschema = new Schema({
  userName: { type: String, unique: true, trim: true, required: true },
  hash: { type: String, trim: true, required: true },
  mobile: { type: String, unique: true, trim: true, required: true },
  role: { type: String, trim: true },
  createdDate: { type: Date, default: Date.now },
  modifiedDate: { type: Date, default: Date.now }
});

const UserRole = new Schema({
  role: { type: String, trim: true, required: true }
});

// schema.set("toJSON", { virtuals: true });

module.exports = mongoose.model("Users", Userschema);
module.exports = mongoose.model("UserRole", UserRole);
